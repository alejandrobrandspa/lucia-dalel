<?php

class Gallery extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'title' => 'required'
	);

	 public function pieces()
    {
        return $this->hasMany('Piece');
    }
}
