App.Models.Piece = Backbone.Model.extend({
	rootUrl: '/api/pieces'
});

App.Collections.Pieces = Backbone.Collection.extend({
	model: App.Models.Piece,
	url: '/api/pieces'
});

