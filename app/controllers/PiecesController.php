<?php

class PiecesController extends BaseController {


	public function index($title = null)
	{	
		$galleries = Gallery::all();
		$title = str_replace('-', ' ', $title);
		$gallery = Gallery::where('title', $title)->first();

		if($gallery) {
			$pieces = Piece::where('gallery_id', $gallery->id)->get();
		} else {
			// $gallery = Gallery::orderBy('id', 'asc')->first();
			// $pieces = Piece::where('gallery_id', $gallery->id)->orderBy('title', 'asc')->get();
			$pieces = Piece::all();
		}

		return View::make('home.index', compact('pieces'), compact('galleries'))->with(compact('title'));
	}
}