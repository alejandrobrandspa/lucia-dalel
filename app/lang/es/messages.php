<?php

return array(
	'subtitle' => 'Cuadros · Obras de Arte · Formatos Personalizados <br> Flores a Gran Escala · Abstracción',
	'intro-1' => 'Primera artista colorista colombiana que cocrea junto con el propietario de la obra, su pieza de arte. Ella logra unir la subjetividad del arte con la necesidad del cliente según su espacio.',
	'intro-2' => '<p>“"Lucía, pudiste interpretar mi deseo y con tu obra quedé feliz, pues es lo que soñaba y como me la había imaginado"</p> <h3>Eucaris Asensio de Pauly</h3>',
	'contact' => ['Contacto', 'Nombre', 'Email', 'Mensaje', 'Enviar'],
	'collections' => 'Colecciones',
	'biography' => ['title' => 'Biografía', 'btn' => 'Ver Más', 'text' => 'Artista colorista fundamentada en terapia del color y formación en armonía cromática'],
	'works' => [
		'2016. Il Giardino di Vera. Mosaico de Hortensias • Colombia • Chile • Italia • Alemania • Estados Unidos • Irlanda • España',
		'2015. Paleta en flor',
		'2014. Colección Sentimientos encapsulados',
		'2013. Colección MARINO’13. Cartagena, Colombia. Hotel Las Américas',
		'2012. Exposición FLORES A GRAN ESCALA. Patrocinado por W&N Londres',
		'2011. Artista Donante COLOMBIA HUMANITARIA. Presidencia de la República',
		'2010. Bienal de Arte. Bruselas, Bélgica. Obra “El florero que nos hizo libres”',
		'Casa del Principado de Asturias. Obra “Mosaico de Hortensias”',
		'2009. Colección ARMONIA CROMATICA. Bogotá, Colombia. Hotel 101 Park House',
		'2007. Subasta Gimnasio Moderno, Obra “Ocaso”. Fundación Emilio de Brigard'
	],
	'copyright' => 'Todos los derechos reservado',
	'mobile' => 'Móvil'
);	