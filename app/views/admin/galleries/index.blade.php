@section('content')	
<a href="/admin/pieces/create" class="btn btn-default">Nueva obra</a>
<a href="/admin/galleries/create" class="btn btn-default">Nueva Galería</a>
<a href="/admin/pieces" class="btn btn-default">Lista obras</a>
<p></p>
<div class="table-responsive">
	<table class="table">
		<tr>
			<th>Titulo</th>
			<th>Obras</th>
			<th>Opciones</th>
		</tr>
		@foreach($galleries as $gallery)
		<tr>
			<td>[[$gallery->title]]</td>
			<td>[[ count($gallery->pieces) ]]</td>
			<td><a href="/admin/galleries/[[$gallery->id]]/edit" class="btn">Editar</a></td>
		</tr>
		@endforeach
	</table>
</div>
@stop