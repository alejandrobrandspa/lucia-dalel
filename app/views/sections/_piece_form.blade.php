<script id="piece-form-template" type="text/x-handlebars-template">
		<div class="form-group">
			<input type="text" name="title" class="form-control" placeholder="Titulo obra" value="{{ title }}">
		</div>
		<div class="form-group">
			<input type="text" name="city" class="form-control" placeholder="Ciudad" value="{{ city }}">
		</div>
		<div class="form-group">
			<input type="text" name="year" class="form-control" placeholder="Año" value="{{ year }}">
		</div>
		<div class="checkbox">
		<label>
		<input type="checkbox" name="available" value="1"> Disponible
		</label>
		</div>
		<button class="btn btn-primary ">Subir</button>

</script>


