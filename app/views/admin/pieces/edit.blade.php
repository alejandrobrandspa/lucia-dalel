@section('content')	
[[ Form::open(array('url' => 'foo/bar', 'method' => 'put'))]]
[[Form::close()]]
<form  action="/admin/pieces/[[$piece->id]]" method="POST" class="piece-form" enctype="multipart/form-data"> 
	<input name="_method" type="hidden" value="PUT">
	@if($piece->gallery)
	<input type="hidden" value="[[ $piece->gallery->title]]">
	@endif
	@include('admin.sections._gallery_select')
	<p></p>
	<div class="form-group">
		<input type="hidden" name="image_name" value="[[$piece->image]]">
		<input type="file" name="image" class="form-control" >
	</div>
	<div class="form-group">
		<input type="text" name="title" class="form-control" placeholder="Titulo obra" value="[[$piece->title]]">
	</div>
	<div class="form-group">
		<input type="text" name="city" class="form-control" placeholder="Tecnica" value="[[$piece->city]]">
	</div>
	<div class="form-group">
		<input type="text" name="measure" class="form-control" placeholder="Medidas" value="[[$piece->measure]]">
	</div>
	<div class="form-group">
		<input type="text" name="year" class="form-control" placeholder="Año" value="[[$piece->year]]">
	</div>
	<div class="checkbox">
		<label>

			@if(!$piece->available)
				<input type="checkbox" name="available" value="1"> Disponible
			@else
			<input type="checkbox" name="available" value="0"> Vendido
			@endif
		</label>
	</div>
	<button type="submit" class="btn btn-primary">Actualizar</button>
	<a href="/admin/pieces" class="btn btn-primary">Cancelar</a>
</form>

	@stop
