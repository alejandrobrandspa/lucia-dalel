/*
|-------------------------------------------------------------------------
|  jquery
|-------------------------------------------------------------------------
*/

$.fn.serializeObject = function() {
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

$.fn.cleanForm = function() {
  var a = this[0].reset();
  return a;
};
/*
|-------------------------------------------------------------------------
|  handlebars
|-------------------------------------------------------------------------
*/
Handlebars.registerHelper('dateFormat', function(context, block) {
  if (window.moment && context && moment(context).isValid()) {
    var f = block.hash.format || "MMM Do, YYYY";
    return moment(context).fromNow();
    } else {
        return context;   //  moment plugin is not available, context does not have a truthy value, or context is not a valid date
    }
});

Handlebars.registerHelper('urlFormat', function(text, block) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp,"<a href='$1' target='_new'>$1</a>");
});

Handlebars.registerHelper('AvailableFormat', function(val, block) {
  
  if(val) {
    return "Disponible";
  } else {
    return "Vendido";
  }

});


/*
|-------------------------------------------------------------------------
|  backbone
|-------------------------------------------------------------------------
*/
Backbone.events = _.extend({}, Backbone.Events);

template = function(templateElement) {
    return Handlebars.compile($(templateElement).html());
};
