<div id="galleryContainer" class="js-masonry">
  @foreach($pieces as $piece)
  <div class="item hide">
    <div class="nailthumb-container square-thumb">
      <a
        href="[[ asset('pieces/'.$piece->image)]]" 
        class="open-photo" 
        title="[[$piece->title]]" 
        data-city="[[$piece->city]] [[$piece->year]]" 
        data-measure="[[$piece->measure]]" 
        data-available=" @if($piece->available) Disponible @else Vendido, pero puede ser parte de la creación del tuyo @endif"
      >
        <img  src="[[ asset('pieces/'.$piece->image)]]" width="170" height="170">
      </a>
    </div>
  </div>
  @endforeach
</div>

<script id="templateGalleryDetail" type="text/x-handlebars-template">
  <div class="first-column">
    <img src="[[ asset('pieces/{{image}}')]]" class="img-responsive">
  </div>
  <div class="second-column">
     <a href="#" class="btn-close" data-dismiss="modal"><img src="[[ asset('img/close.png') ]]" alt=""></a>
    <h3 class="piece-title">{{title}}</h3>
    <h4 class="piece-year">{{city}} {{year}}</h4>
    <h4 class="piece-year">{{measure}}</h4>
    <span class="piece-status">Disponible</span>
<div class="modal-btns">
    <a href="#" class="btn btn-open-contact">Contacto</a>
    <br>
    <br>
    <a href="#" class="btn btn-next-piece" data-piece-id="{{id }}">Siguiente</a>
    </div>
  </div>
</script>

