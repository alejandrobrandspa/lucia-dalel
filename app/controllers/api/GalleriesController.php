<?php namespace Api;

use Gallery;
use Input;
use View;
use Validator;
use Response;

class GalleriesController extends \BaseController {

	/**
	 * Gallery Repository
	 *
	 * @var Gallery
	 */
	protected $gallery;

	public function __construct(Gallery $gallery)
	{
		$this->gallery = $gallery;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return $this->gallery->all();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('galleries.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Gallery::$rules);

		if ($validation->passes())
		{
			$gallery = $this->gallery->create($input);

			return Response::json($gallery, '200');
		}

		return Response::json($validation, '200');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return $this->gallery->findOrFail($id);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Gallery::$rules);

		if ($validation->passes())
		{
			$gallery = $this->gallery->find($id);
			$gallery->update($input);

			return Response::json('galleries.show', $id);
		}

		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return $this->gallery->find($id)->delete();
	}

}
