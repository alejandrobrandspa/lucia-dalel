module.exports = function(grunt) {

	grunt.initConfig({
		concat: {
			libs: {
				src: [
				"./js/libs/jquery.min.js'",
				"./js/libs/bootstrap.min.js",
				"./js/libs/underscore-min.js",
				"./js/libs/backbone-min.js",
				"./js/libs/handlebars-v1.1.2.js",
				"./vendor/alertify/alertify.min.js",
				"./js/plugins/masonry.pkgd.min.js",
				"./js/plugins/jquery.magnific-popup.js",
				],
				dest: "./js/libs.js"
			},
			app: {
				src: [
					"./js/app.js",
					"./js/helpers.js",

					],
				dest: "./js/build.js"
			}
		},

		uglify: {
			libs: {
				src: "./js/libs.js",
				dest: './js/libs.min.js'
			},
			app: {
				src: "./js/build.js",
				dest: './js/build.min.js'
			}
		}
		});

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask("default", [ "concat", "uglify"]);
   grunt.registerTask("dist", [ "default" ]);
};
