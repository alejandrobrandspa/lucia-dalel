<?php namespace Admin;

use Piece;
use Gallery;
use Input;
use View;
use Validator;
use Response;
use Session;
use Redirect;

class PiecesController extends \BaseController {

	protected $layout = "layouts.admin";
	protected $piece;

	public function __construct(Piece $piece, Gallery $gallery)
	{
		$this->piece = $piece;
		$this->gallery = $gallery;
	}

	public function index()
	{
		$galleries = $this->gallery->all(); 
		$pieces = $this->piece->with('gallery')->get();
		$this->layout->content = View::make('admin.pieces.index', compact('pieces'), compact('galleries'));
	}

	public function create()
	{
		$galleries = $this->gallery->all(); 
      	$this->layout->content = View::make('admin.pieces.create' , compact('galleries'));
	}

	public function store()
	{
		if (Input::hasFile('image')) {
			$piece = new Piece;
			$path = public_path()."/pieces";
			$file = Input::file('image');
			$ext = $file->getClientOriginalExtension();
			$name = "piece-".str_random(25).'.'.$ext;
			$piece->gallery_id = Input::get('gallery_id');
			$piece->title = Input::get('title');
			$piece->image = $name;
			$piece->city = Input::get('city');
			$piece->measure = Input::get('measure');
			$piece->year = Input::get('year');
			$piece->available = (Input::get('available') == null ? false : true);
			$file->move($path,$name);
			$piece->save();
			return Redirect::to('/admin/pieces');
		}

	}

	public function update($id)
	{
		$name = Input::get('image_name');

		if (Input::hasFile('image')) {
			$path = public_path()."/pieces";
			$file = Input::file('image');
			$ext = $file->getClientOriginalExtension();
			$name = "piece-".str_random(25).'.'.$ext;
			$file->move($path,$name);		
		}

		$piece = $this->piece->find($id);
		$piece->gallery_id = Input::get('gallery_id');
		$piece->title = Input::get('title');
		$piece->image = $name;
		$piece->city = Input::get('city');
		$piece->measure = Input::get('measure');
		$piece->year = Input::get('year');
		$piece->available = Input::get('available');
		$piece->save();
		return Redirect::to('/admin/pieces');
	}

	public function edit($id) 
	{
		$piece = $this->piece->find($id);
		$galleries = $this->gallery->all();
		$this->layout->content = View::make('admin.pieces.edit', compact('piece'), compact('galleries'));
	}

	public function destroy($id)
	{
		$piece = $this->piece->find($id);
		$piece->delete();
		return Redirect::to('/admin/pieces');
	}
}
