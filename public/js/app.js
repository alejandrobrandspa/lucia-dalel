$(document).on('ready', function(){
   
    var total = $("#galleryContainer").find('.item').length;
    var itemThumbs = 0;
     $('.item').removeClass('hide');
    $('.nailthumb-container').nailthumb({
        onFinish: function() {
            itemThumbs += 1;
        }
    });
   
    var mason = function() {
        $("#galleryContainer").masonry({
        itemSelector: '.item',
        columnWidth: 140,
        "gutter": 10
        });
    }
       
});

$(function() {
    $('#galleryContainer').magnificPopup({
        type: 'image',
        delegate: 'a',
        gallery: {
            enabled: true
        }
    });

    $('.contact-store').on('click', function(e) {
        e.preventDefault();
        $this = $(this);
        form = $this.parent();
        model = form.serialize();
        promise = $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: model,
            dataType: 'json',
            beforeSend: function(){
              $this.text('Enviando');
          }
        });

        promise.done(function(model){
            if(model.created_at) {
               form.fadeOut();
               $('.container-form').append("<h4>Mensaje enviado con exito</h4>");
            } else {
                $this.text('Enviar');
                $.each(model, function(key, val){
                    alertify.error(val);
                });
            }
        });
    });
});

$.extend(true, $.magnificPopup.defaults, {
    tClose: 'Cerrar',
    tLoading: 'Cargando...',
    gallery: {
        tPrev: 'Anterior',
        tNext: 'Siguiente',
        tCounter: ''
    },
    image: {
        tError: '<a href="%url%">La imagen</a> no pudo ser cargada.'
    },
    ajax: {
        tError: '<a href="%url%">The content</a> could not be loaded.'
    }
});


var App = {
    Models: {},
    Views: {},
    Collections: {}
};
