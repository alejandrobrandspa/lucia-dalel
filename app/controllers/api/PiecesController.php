<?php namespace Api;

use Piece;
use Gallery;
use Input;
use View;
use Validator;
use Response;
use Session;
use Redirect;

class PiecesController extends \BaseController {

	protected $piece;

	public function __construct(Piece $piece, Gallery $gallery)
	{
		$this->piece = $piece;
		$this->gallery = $gallery;
	}

	public function index()
	{
      $pieces =  $this->piece->with('gallery')->get();
      return Response::json($pieces);
	}


	public function create()
	{
        
	}

	/*public function store()
	{
		$input = Input::all();
		$piece = $this->piece->create($input);

		if($piece) {
			$id = $piece->id;
			$images = Session::get('piece.images');
			if($images) {
				foreach ($images as $key => $val) {
					$this->image->create(['piece_id' => $id, 'name' => $val]);
				}
				Session::forget('piece.images');
			}
		}

		$images = $piece->images->toArray();
		$pi = $piece->toArray();

		return Response::json([$pi, $images]);
	}*/

	public function store() 
	{
		$piece = new Piece;
		$path = public_path()."/pieces";
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "piece-".str_random(25).'.'.$ext;
		$piece->gallery_id = Input::get('gallery_id');
		$piece->title = Input::get('title');
		$piece->image = $name;
		$piece->city = Input::get('city');
		$piece->city = Input::get('measure');
		$piece->city = Input::get('measure');
		$piece->year = Input::get('year');
		$piece->available = (Input::get('available') == null ? false : true);

		$file->move($path,$name);
		$piece->save();		
		$response = '<textarea data-type="application/json">'. json_encode($piece->toArray()).'</textarea>';
		return $response;
	}

	public function show($id)
	{
       $piece =  $this->piece->find($id);
       $pieceArray =  $this->piece->find($id)->toArray();
       $gallery = $piece->gallery->toArray();
       $p = array_merge($pieceArray, $gallery);
       return Response::json([$pieceArray, $gallery]);
	}

	
	public function edit($id)
	{
       
	}

	public function update($id)
	{
		$name = Input::get('image');

		if (Input::hasFile('image')) {
			$path = public_path()."/pieces";
			$file = Input::file('image');
			$ext = $file->getClientOriginalExtension();
			$name = "piece-".str_random(25).'.'.$ext;
			$file->move($path,$name);		
		}

		$piece = $this->piece->find($id);
		$piece->gallery_id = Input::get('gallery_id');
		$piece->title = Input::get('title');
		$piece->image = $name;
		$piece->city = Input::get('city');
		$piece->city = Input::get('measure');
		$piece->year = Input::get('year');
		$piece->available = (Input::get('available') == null ? false : true);
		$piece->save();
		$response = '<textarea data-type="application/json">'. json_encode($piece->toArray()).'</textarea>';
		return $response;
	}

	public function destroy($id)
	{
		$model = $this->piece->find($id);
		$model->delete();

	}

}
