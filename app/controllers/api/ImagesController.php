<?php namespace Api;

use Input;
use Response;
use Session;

class ImagesController extends \BaseController {

	protected $image;
	public function __construct(\Image $image)
	{
		$this->image = $image;
	}

	public function index()
	{
		return $this->image->all();
	}

	public function store()
	{
		$path = public_path()."/pieces";
		$file = Input::file('file');
		$ext = $file->getClientOriginalExtension();
		$name = "piece-".str_random(25).'.'.$ext;
		$file->move($path,$name);
		// Session::forget('piece.images');
		Session::push('piece.images', $name);
		$imgs = Session::get('piece.images');
		return Response::json($imgs);
	}
}