<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Lucia Dalel / [[ $title ]]</title>
	<meta name="description" content="Primera artista Colombiana Colorista que cocrea junto con el propietario de la obra, su pieza de arte. Logrando unir la subjetividad del ARTE, sin dejar de lado la necesidad del cliente a través del DISEÑO. A esto lo llama Arte en Maletín.">
	<link rel="shortcut icon" href="[[asset('favicon.ico')]]">
	<link rel="stylesheet" href="[[ asset('css/bootstrap.min.css') ]]">
	<link rel="stylesheet" href="[[ asset('css/magnific-popup.css') ]]">
	<link rel="stylesheet" href="[[ asset('css/main.css') ]]">
	<link rel="stylesheet" href="[[ asset('vendor/alertify/themes/alertify.core.css') ]]">
	<link rel="stylesheet" href="[[ asset('vendor/alertify/themes/alertify.default.css') ]]">
	<style>
		body {
			background-size: 'cover';
		}
	</style>
</head>
<body>

	<div class="container">

	@if(Request::segment(1) == "en")
		<a href="/" class="pull-right"><img style="margin-top: 7px" src="/img/flag-es.png" alt=""></a>
	@else
		<a href="/en" class="pull-right"><img style="margin-top: 7px" src="/img/flag-en.png" alt=""></a>
	@endif

	<div class="pull-right" style="margin-right: 15px">	<a style="color: #4f6a70; font-size: 20px" href="http://blog.luciadalel.com" target="new">Prensa </a></div>

		<div id="header">
			<img src="[[asset('img/firm.png')]]" alt="" class="firm">
			<img src="[[asset('img/photo.jpg')]]" alt="" class="photo hidden-sm hidden-xs">
			<div class="title hidden-md hidden-sm hidden-xs">
				<h1> LUCÍA DALEL </h1>

				<h3 class="subtitle">[[ Lang::get('messages.subtitle') ]]</h3>


			</div>
		</div>

		@yield('content')

		<div id="footer">
			<ul class="social pull-right">
				<li><a href="https://www.facebook.com/LDalelN7" target="new"><img src="[[ asset('img/fb.png') ]]" alt=""></a></li>
				<li><a href="https://twitter.com/luciadalel" target="new"><img src="[[ asset('img/tw.png') ]]" alt=""></a></li>
			</ul>
			<div class="pull-right">
				<p>[[ Lang::get('messages.copyright') ]]  - Lucía Dalel 2013 -</p>
				<p>[[ Lang::get('messages.mobile') ]]: (57) 312-5115-973 - Email: info@luciadalel.com</p>
			</div>

		</div>
	</div>

	<!-- libs -->
	<script src="[[ asset('js/libs/jquery.min.js') ]]"></script>
	<script src="[[ asset('js/libs/bootstrap.min.js') ]]"></script>
	<script src="[[ asset('js/libs/underscore-min.js') ]]"></script>
	<script src="[[ asset('js/libs/backbone-min.js') ]]"></script>
	<script src="[[ asset('js/libs/handlebars-v1.1.2.js') ]]"></script>
	<script src="[[ asset('vendor/alertify/alertify.min.js') ]]"></script>
	<!-- plugins -->
	<script src="[[ asset('js/plugins/jquery.nailthumb.1.1.min.js') ]]"></script>
	<script src="[[ asset('js/plugins/masonry.pkgd.min.js') ]]"></script>
	<script src="[[ asset('js/plugins/jquery.magnific-popup.js') ]]"></script>
	<script src="[[ asset('js/plugins/jquery.lazyload.min.js') ]]"></script>

	<!-- scripts -->
	<script src="[[ asset('js/app.js') ]]"></script>
	<script src="[[ asset('js/helpers.js') ]]"></script>
	<script>
	 $("img.lazy").lazyload({
	 	 effect : "fadeIn"
	 });
	</script>
	@section('scripts')
	@show


</body>
</html>
