<?php namespace Api;

use Validator;
use Input;
use Response;
use Contact;
use Mail;

class ContactController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$validator = Validator::make($inputs, Contact::$rules);
		if ($validator->passes()) {
			$contact = Contact::create($inputs);
			$data = ['name' => $contact->name, 'email' => $contact->email, 'text' => $contact->message];

			Mail::send('emails.welcome', $data, function($message)
			{
				$message->to('alejandro@brandspa.com', 'Lucía dalel')->subject('Contacto lucidaldel.com');
				$message->to('info@luciadalel.com', 'Lucía dalel')->subject('Contacto lucidaldel.com');
			});

			return Response::json($contact);
		}

		return Response::json($validator->messages());
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
