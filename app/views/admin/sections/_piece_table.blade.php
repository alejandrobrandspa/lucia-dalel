<script id="piece-row-template" type="text/x-handlebars-template">
	<td>
	<img src="[[ asset('pieces/{{image}}' ) ]]" alt="{{image}}" width="50">
	</td>
	<td>{{title}}</td>
	<td>{{year}}</td>
	<td>{{city}}</td>
	<td>
	<a href="#" class="btn btn-default piece-available" data-piece-available="{{available}}">{{AvailableFormat available}}</a>
	</td>
	<td>
	<a href="#" class="btn btn-default piece-edit">Editar</a>
	<a href="#" class="btn btn-default piece-remove">Eliminar</a>
	</td>
</script>