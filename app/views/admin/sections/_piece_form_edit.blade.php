<script id="piece-form-edit-template" type="text/x-handlebars-template">

		<p></p>
		<img src="[[ asset('pieces/{{image}}') ]]" alt="" width="200" class="img-responsive">
		<p></p>
		<input type="hidden" name="id" value="{{id}}">
		<div class="form-group">
			<input type="file" name="image" class="form-control">
			<input type="hidden" name="image" value="{{image}}">
		</div>
		<div class="form-group">
			<input type="text" name="title" class="form-control" placeholder="Titulo obra" value="{{title}}">
		</div>
		<div class="form-group">
			<input type="text" name="city" class="form-control" placeholder="Ciudad" value="{{city}}">
		</div>
		<div class="form-group">
			<input type="text" name="year" class="form-control" placeholder="Año" value="{{year}}">
		</div>
		<a href="#" class="btn btn-default piece-edit">Actualizar</a>
		<a href="#" class="btn btn-default close-form">Cancelar</a>
</script>
