<select name="gallery_id">
	@if(isset($piece->gallery) && $piece->gallery->title)
		<option value="[[ $piece->gallery->id ]]">[[ $piece->gallery->title ]]</option>
	@else
		<option value="0">Seleccionar galería</option>
	@endif

	@foreach($galleries as $gallery)
	<option value="[[ $gallery->id ]]">[[ $gallery->title ]]</option>
	@endforeach
</select>