<?php

return array(

	'subtitle' => 'Paintings · Artworks · Custom Formats <br> Large Scale Flowers · Abstraction',
	'intro-1' => 'First colorist artist from Colombia that co-creates along with the owner of the work, the piece of art. She manages to bring together the subjectivity of art with the customer need in accordance with his space.',
	'intro-2' => '<p>“Lucía brought art to my home, she lived the space, read my need and translate it to an artwork that complemented my home, transforming this space”</p> <h3>Elvira Rotschild</h3>',
	'contact' => ['Contact', 'Name', 'Email', 'Message', 'Send'],
	'collections' => 'Collections',
	'biography' => ['title' => 'Biography', 'btn' => 'See More', 'text' => 'Colorist artist, based on color therapy and training in color harmony'],
	'works' => [
		'2013. Collection MARINO’13. Cartagena, Colombia. Hotel Las Américas',
		'2012. FLORES A GRAN ESCALA Exhibition. Sponsored by W&N London',
		'2011. Donor Artist HUMANITARIAN COLOMBIA. Presidency of the Republic',
		'2010. Art Biennale. Brussels, Belgium. Artwork “El florero que nos hizo libres”',
		'Principality House of Asturias. Artwork “Mosaico de Hortensias”',
		'2009. ARMONIA CROMATICA Collection. Bogotá, Colombia. 101 Park House Hotel',
		'2007. Auction Gimnasio Moderno, Artwork “Ocaso”. Emilio de Brigard Foundation'
	],
	'copyright' => 'All rights reserved',
	'mobile' => 'Mobile'
);	