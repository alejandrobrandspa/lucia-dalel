@section('content')	

<form  action="/admin/pieces" method="POST" class="piece-form" enctype="multipart/form-data"> 
@include('admin.sections._gallery_select')
	<p></p>
	<div class="form-group">
		<input type="file" name="image" class="form-control" >
	</div>
	<div class="form-group">
		<input type="text" name="title" class="form-control" placeholder="Titulo obra" >
	</div>
	<div class="form-group">
		<input type="text" name="city" class="form-control" placeholder="Ciudad" >
	</div>
	<div class="form-group">
		<input type="text" name="measure" class="form-control" placeholder="Medidas" >
	</div>
	<div class="form-group">
		<input type="text" name="year" class="form-control" placeholder="Año" >
	</div>
	<div class="checkbox">
		<label>
			<input type="checkbox" name="available" value="1"> Disponible
		</label>
	</div>
	<button type="submit" class="btn btn-primary">Guardar</button>
	<a href="/admin/pieces" class="btn btn-primary">Cancelar</a>
</form>

	@stop