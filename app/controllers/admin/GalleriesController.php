<?php namespace Admin;

use Gallery;
use Piece;
use Input;
use View;
use Validator;
use Response;
use Session;
use Redirect;

class GalleriesController extends \BaseController {

	protected $layout = "layouts.admin";
	protected $gallery;

	public function __construct(Gallery $gallery)
	{
		$this->gallery = $gallery;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$galleries = $this->gallery->all();
		$this->layout->content = View::make('admin.galleries.index', compact('galleries'));
	}

	public function create() 
	{
		$galleries = $this->gallery->all();
		$this->layout->content = View::make('admin.galleries.create', compact('galleries'));
	}

	public function store() 
	{
		$gallery = new Gallery;
		$gallery->title = Input::get('title');
		$gallery->save();
		return Redirect::to('/admin/pieces');
	}

	public function edit($id) 
	{
		$gallery = $this->gallery->find($id);
		$this->layout->content = View::make('admin.galleries.edit', compact('gallery'));
	}

	public function update($id) 
	{
		$gallery = $this->gallery->find($id);
		$gallery->title = Input::get('title');
		$gallery->save();
		return Redirect::to('/admin/pieces');
	}

	public function destroy($id)
	{
		$gallery = $this->gallery->find($id);
		$gallery->delete();
		return Redirect::to('/admin/pieces');
	}


}
