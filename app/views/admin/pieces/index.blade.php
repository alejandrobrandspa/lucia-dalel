@section('content')
<a href="/admin/pieces/create" class="btn btn-default">Nueva obra</a>
<a href="/admin/galleries/create" class="btn btn-default">Nueva Galería</a>
<a href="/admin/galleries" class="btn btn-default">Lista galerías</a>
<p></p>
<div class="table-responsive">
<table class="table">
	<tr>
		<th>Imagen</th>
		<th>Titulo</th>
		<th>Tecnica</th>
		<th>Medidas</th>
		<th>Año</th>
		<th>Disponibilidad</th>
		<th>Opciones</th>
	</tr>
	@foreach($pieces as $piece)
	<tr>
		<td><img src="[[asset('pieces/'.$piece->image)]]" alt="" width="50"></td>
		<td>[[$piece->title]]</td>
		<td>[[$piece->city]]</td>
		<td>[[$piece->measure]]</td>
		<td>[[$piece->year]]</td>
		<td>
			@if($piece->available)
				Disponible
			@else
				Vendido
			@endif
		</td>
		<td>
			<a href="/admin/pieces/[[$piece->id]]/edit" class="btn btn-sm btn-default">Editar</a>
			<p></p>
			<form action="/admin/pieces/[[$piece->id]]" method="POST">
				<input name="_method" type="hidden" value="DELETE">
				<button type="submit" class="btn btn-sm">Eliminar</button>
			</form>
		</td>
	</tr>
	@endforeach
</table>
</div>
@stop

@section('scripts')
<script src="[[ asset('js/gallery.js') ]]"></script>
<script src="[[ asset('js/piece.js') ]]"></script>
@stop