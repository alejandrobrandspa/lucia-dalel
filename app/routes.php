<?php
$locale = Request::segment(1);

if ($locale == 'en') {
	App::setLocale('en');
}

Route::filter('etags', function($route, $request, $response)
{
  $response->setEtag(md5($response->getContent()));
	$response->isNotModified($request);
	return;
});

Route::group(['after' => 'etags'], function(){
	Route::get('/{gallery_id?}', 'PiecesController@index');

Route::group(['prefix' => 'en'], function(){
	Route::get('/{gallery_id?}', 'PiecesController@index');
});

});


Route::group(['prefix' =>'admin','before' => 'auth.basic'], function(){
	route::resource('pieces', 'Admin\PiecesController');
	route::resource('galleries', 'Admin\GalleriesController');
});

Route::group(['prefix' =>'api', 'after' => 'etags'], function(){
	Route::resource('galleries', 'Api\GalleriesController');
	Route::resource('pieces', 'Api\PiecesController');
	Route::post('pieces/{id}', 'Api\PiecesController@update');
	Route::resource('contacts', 'Api\ContactController');
});

