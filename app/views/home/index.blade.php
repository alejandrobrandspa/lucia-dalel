@extends('layouts.main')
@section('content')
<style>
	body {
			background-size: 'cover';
		}
	.sub-header {
		position: relative;
		background: #fff;
		margin-bottom: 15px;
		overflow: hidden;
		border: 15px solid #fff;
	}

	.sub-header img {
		width: 100%;
	}

	.sub-header__content {
		position: absolute;
		background: rgba(0,0,0, .5);
		color: #fff;
		top: 0;
		left: 0;
		width: 400px;
		height: 100%;
		padding: 35px;
	}

	.sub-header__content h3 {
		font-size: 36px;
		font-style: italic;
	}
	.sub-header__content h5 {
		font-size: 18px;
	}

	.sub-header__content span {
		display: block;
		margin-top: 35px;
	}

	.sub-header__content p {
		font-size: 14px;
		font-style: regular;
		font-family: 'Crimson Text', serif;
	}

</style>
	<div class="sub-header">
		<img src="/img/lucia.jpg" class="img-responsive" alt="">
		<div class="sub-header__content">
			<h3>Exhibición Vigente</h3>
			<h5>Bogotá, Colombia</h5>
			<span>
				<p>Calle de Los Anticuarios</p>
				<p>Carrera 9 No. 79B – 64 Esquina</p>
				<p>Cita Previa 3125115973</p>
		</span>
		</div>
	</div>
	

	<div id="intro">
		<div class="col-lg-2">
			<div class="intro-box-1">
				<p>
					Taylor Made FineArt
				</p>
					<a href="mailto:info@luciadalel.com" style="color:#fff; display: block; text-align: right; padding-left:35px ">Agendar fecha disponible de creación de obra</a>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="intro-box-2">
				<p>
					[[ Lang::get('messages.intro-1') ]]
				</p>
			</div>
		</div>

		<div class="col-lg-5">
			<div class="intro-box-3">
			[[ Lang::get('messages.intro-2') ]]
			</div>
		</div>
	</div>

	<div class="col-lg-8">
		<div class="container-gallery">
			<div class="gallery-inside">
			<div class="btn-group btn-select-gallery">
				<button class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown">
					[[ Lang::get('messages.collections') ]]
				</button>
				<ul class="dropdown-menu">
					@foreach($galleries as $gallery)
					<?php $locale = Request::segment(1); ?>
						<li>
							<a href="  @if($locale == 'en') /en/[[ str_replace(' ', '-', $gallery->title) ]] @else /[[ str_replace(' ', '-', $gallery->title) ]] @endif ">[[ $gallery->title ]]</a>
						</li>
					@endforeach
				</ul>
			</div>
			
			@include('home.sections._gallery')
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="container-form">
			<h3>[[ Lang::get('messages.contact.0') ]]</h3>
			<form action="/api/contacts" class="form-horizontal">
				<div class="form-group">
					<label for="name" class="control-label col-sm-3">[[ Lang::get('messages.contact.1') ]]:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="name">
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="control-label col-sm-3">[[ Lang::get('messages.contact.2') ]]:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="email">
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="control-label col-sm-3">[[ Lang::get('messages.contact.3') ]]:</label>
					<div class="col-sm-9">
						<textarea name="message" class="form-control" rows="1"></textarea>
					</div>
				</div>
				<a href="#" class="btn btn-send contact-store">[[ Lang::get('messages.contact.4') ]]</a>
			</form>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="container-bio">
			<h3>[[ Lang::get('messages.biography.title') ]]</h3>
			<p>
				[[ Lang::get('messages.works.0') ]]
			</p>
			<p>
				[[ Lang::get('messages.works.2') ]]
			</p>
			<button type="button" class="btn btn-more" data-toggle="modal" data-target="#modal-bio">[[ Lang::get('messages.biography.btn') ]]</button>
		</div>
	</div>


	<div class="modal fade" id="modal-bio">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					 <img src="[[asset('img/photo.png')]]" alt="">
				</div>
				<div class="modal-body">
					<h4>LUCIA DALEL NIETO. 1984, Bogotá Colombia</h4>
					<p></p>
					<p>[[ Lang::get('messages.biography.text') ]]</p>

					<ul>
						<li>[[ Lang::get('messages.works.0') ]]</li>
						<li>[[ Lang::get('messages.works.1') ]]</li>
						<li>[[ Lang::get('messages.works.2') ]]</li>
						<li>[[ Lang::get('messages.works.3') ]]</li>
						<li>[[ Lang::get('messages.works.4') ]]</li>
						<li>[[ Lang::get('messages.works.5') ]]</li>
						<li>[[ Lang::get('messages.works.6') ]]</li>
						<li>[[ Lang::get('messages.works.7') ]]</li>
						<li>[[ Lang::get('messages.works.8') ]]</li>
						<li>[[ Lang::get('messages.works.9') ]]</li>
					</ul>
				</div>

			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('scripts')
<script src="[[ asset('js/piece.js') ]]"></script>
<script>

</script>
@stop

