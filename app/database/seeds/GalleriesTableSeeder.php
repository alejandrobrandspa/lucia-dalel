<?php

class GalleriesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('galleries')->truncate();

		$galleries = array(
			['title' => 'Colección Azules 2009'],
			['title' => 'Colección Verdes 2010'],
			['title' => 'Colección Amarillos 2011'],
			['title' => 'Colección Azules 2012']
		);

		// Uncomment the below to run the seeder
		DB::table('galleries')->insert($galleries);
	}

}
