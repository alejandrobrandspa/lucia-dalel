<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lucia Dalel</title>
	<link rel="stylesheet" href="[[ asset('css/bootstrap.min.css') ]]">
	<link rel="stylesheet" href="[[ asset('css/main.css') ]]">

</head>
<body>
	<div class="container">
		<div id="header">
			<img src="[[asset('img/firm.png')]]" alt="" class="firm">
			<img src="[[asset('img/photo.png')]]" alt="" class="photo hidden-sm hidden-xs">
			<div class="title hidden-sm hidden-xs">
				<h1> LUCÍA DALEL </h1>
				<span class="subtitle">Taylor Made Pieces</span>
			</div>
		</div>
		@yield('content')
	</div>
		
	<!-- libs -->
	<script src="[[ asset('js/libs/jquery.min.js') ]]"></script>
	<script src="[[ asset('js/libs/bootstrap.min.js') ]]"></script>
	<script src="[[ asset('js/libs/underscore-min.js') ]]"></script>
	<script src="[[ asset('js/libs/backbone-min.js') ]]"></script>
	<script src="[[ asset('js/libs/handlebars-v1.1.2.js') ]]"></script>
	<!-- plugins -->
	<script src="[[ asset('js/plugins/jquery.iframe-transport.js') ]]"></script>
	<script src="[[ asset('js/plugins/masonry.pkgd.min.js') ]]"></script>
	<!-- scripts -->
	<script src="[[ asset('js/app.js') ]]"></script>
	<script src="[[ asset('js/helpers.js') ]]"></script>
	@section('scripts')
	@show
</body>
</html>