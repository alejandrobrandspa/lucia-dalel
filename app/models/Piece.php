<?php

class Piece extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

	public function gallery()
	{
		return $this->belongsTo('Gallery');
	}

}
