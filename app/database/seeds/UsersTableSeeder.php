<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('users')->truncate();

		$users = array(
			['email' => 'contacto@luciadalel.com', 'password' => Hash::make('Luc1AD4l3L2013')]
		);

		// Uncomment the below to run the seeder
		DB::table('users')->insert($users);
	}

}
